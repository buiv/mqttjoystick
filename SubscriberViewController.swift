//
//  SubscriberViewController.swift
//  MQTTJoyStick
//
//  Created by vichenka on 11/30/15.
//  Copyright © 2015 jmesnil.net. All rights reserved.
//

import UIKit

class SubscriberViewController: UIViewController {
    var label = UILabel()
    var firstAppear = true
    @IBOutlet weak var clickJoyStickLabel: UILabel!

    @objc var draggingLabel: String = ""
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "azure.jpg")!)
        //NSLog("Simulator label: %@", draggingLabel)
        // Do any additional setup after loading the view, typically from a nib.
        
        label = UILabel(frame: CGRectMake(self.view.bounds.width / 2 - 100, self.view.bounds.height / 2 - 50, 200, 100))
        label.text = draggingLabel
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        self.view.addSubview(label)
        
        clickJoyStickLabel.textColor = UIColor.whiteColor()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "coordinateChanged:", name: "DataUpdated", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "coordinateChangedToOrigin:", name: "CoordinateChangedToOrigin", object: "0.00,0.00")
        
        
    }
    
//    override func viewDidAppear(animated: Bool) {
//       label.center = CGPoint(x: innerView.bounds.width / 2, y: innerView.bounds.height / 2)
//    }
    
    func coordinateChanged(notification: NSNotification){
        //print("OK Notified")
        if firstAppear == true {
            firstAppear = false
            return
        }
        
        
        //received new translation
        
        let newTranslation = notification.object as! String
        
        let arr = newTranslation.componentsSeparatedByString(",")
        
        let newTranslationX = (arr[0] as NSString).floatValue
        
        let newTranslationY = (arr[1] as NSString).floatValue
        
        label.center = CGPoint(x: self.view.bounds.width / 2 + CGFloat(newTranslationX), y: self.view.bounds.height / 2 + CGFloat(newTranslationY))
        //label.center = CGPoint(x: CGFloat(newX), y: CGFloat(newY))
        //print(newTranslationX, newTranslationY)
        //print(label.center.x, label.center.y)
        
        let xFromCenter = label.center.x - self.view.bounds.width / 2
        
        let scale = min(50 / abs(xFromCenter), 1)
        
        
        var rotation = CGAffineTransformMakeRotation(xFromCenter / 200)
        
        var stretch = CGAffineTransformScale(rotation, scale, scale)
        
        label.transform = stretch
        
        
    }
    
    func coordinateChangedToOrigin(notification: NSNotification){
        label.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "showJoyStick") {
            //let navigationController = segue.destinationViewController as! UINavigationController
            let vc = segue.destinationViewController as! JoyStickViewController
            vc.draggingLabel = "Drag me!!"
        }
    }


}
