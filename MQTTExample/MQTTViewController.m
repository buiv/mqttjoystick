//
//  MQTTViewController.m
//  MQTTExample
//
//  Modified by vichenka on 11/29/15.
//  Copyright (c) 2014 jmesnil.net. All rights reserved.
//

#import "MQTTViewController.h"
#import <MQTTKit.h>

#import "MQTTJoyStick-Swift.h"

#define kMQTTServerHost @"iot.eclipse.org"
#define kTopic @"MQTTJoyStick/One"

@interface MQTTViewController ()

@property (weak, nonatomic) IBOutlet UITextField *brokerText;
@property (weak, nonatomic) IBOutlet UITextField *topicText;


// create a property for the MQTTClient that is used to send and receive the message
@property (nonatomic, strong) MQTTClient *client;

@end

@implementation MQTTViewController

BOOL connected = false;
NSString *clientID;
- (void)viewDidLoad
{
    UIImage *backgroundImage = [UIImage imageNamed:@"background1.png"];
    UIImageView *backgroundImageView=[[UIImageView alloc]initWithFrame:self.view.frame];
    backgroundImageView.image=backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
    
    
    [super viewDidLoad];
    
    //set default value
    [self.brokerText setText:kMQTTServerHost];
    [self.topicText setText:kTopic];
    [self.brokerText setEnabled:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coordinateChanged:)
                                                 name:@"CoordinateChanged"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(coordinateChangedToOrigin:)
                                                 name:@"CoordinateChangedToOrigin"
                                               object:nil];
    
}


- (void)viewDidAppear:(BOOL)animated {
    if (connected == true){
        NSLog(@"disconnect now");
        [self disconnect];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Broker Disconnected"
                                                        message:@"Reconnect to subcribe to topic"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [self performSelector:@selector(test:) withObject:alert afterDelay:2];
    }
}

- (void) test:(UIAlertView*)alert {
    [alert dismissWithClickedButtonIndex:-1 animated:TRUE];
}

-(void)dismissKeyboard {
    [self.view endEditing:true];
    //[aTextField resignFirstResponder];
}

-(void)coordinateChanged:(NSNotification *)notification {
    NSString *center = [notification object];
    //NSLog(@"Console recieved %@", center);
    
    [self.client publishString:center
                       toTopic:self.topicText.text
                       withQos:AtMostOnce
                        retain:YES
             completionHandler:nil];
}

-(void) connect {
    // create the MQTT client with an unique identifier
    clientID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    self.client = [[MQTTClient alloc] initWithClientId:clientID];
    
    
    
    // define the handler that will be called when MQTT messages are received by the client
    [self.client setMessageHandler:^(MQTTMessage *message) {
        // extract the switch status from the message payload
        
        
        // the MQTTClientDelegate methods are called from a GCD queue.
        // Any update to the UI must be done on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *newcenter = message.payloadString;
            //NSLog(@"newcenter: %@", newcenter);
            //            NSString *newcenter = @"118.50-119.50";
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                                object:newcenter];
        });
    }];
    
    // connect the MQTT client
    [self.client connectToHost:self.brokerText.text completionHandler:^(MQTTConnectionReturnCode code) {
        if (code == ConnectionAccepted) {
            connected = true;
            // The client is connected when this completion handler is called
            NSLog(@"client is connected with id %@", clientID);
            // Subscribe to the topic
            [self.client subscribe:self.topicText.text withCompletionHandler:^(NSArray *grantedQos) {
                // The client is effectively subscribed to the topic when this completion handler is called
                NSLog(@"subscribed to topic %@", self.topicText.text);
            }];
        }
    }];
}

- (void)disconnect {
    // disconnect the MQTT client
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        // The client is disconnected when this completion handler is called
        NSLog(@"MQTT is disconnected");
        connected = false;
    }];
}

- (void)dealloc
{
    // disconnect the MQTT client
    [self.client disconnectWithCompletionHandler:^(NSUInteger code) {
        // The client is disconnected when this completion handler is called
        NSLog(@"MQTT is disconnected");
        connected =false;
    }];
}

#pragma mark - IBActions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    [self connect];
    
    if ([segue.identifier isEqualToString:@"showSubscriber"]) {
        SubscriberViewController *destViewController = segue.destinationViewController;
        destViewController.draggingLabel = @"Drag me NOT! ";

    }
}

@end
