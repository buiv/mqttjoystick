//
//  JoyStickViewController.swift
//  MQTT Connector
//
//  Created by vichenka on 11/29/15.
//  Copyright © 2015 jmesnil.net. All rights reserved.
//

import UIKit



@objc class JoyStickViewController: UIViewController {

    var draggingLabel: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "azure.jpg")!)
        NSLog("label: %@", draggingLabel)
        // Do any additional setup after loading the view, typically from a nib.
        
        let label = UILabel(frame: CGRectMake(self.view.bounds.width / 2 - 100, self.view.bounds.height / 2 - 50, 200, 100))
        label.text = draggingLabel
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        self.view.addSubview(label)
        
        let gesture = UIPanGestureRecognizer(target: self, action: Selector("wasDragged:"))
        label.addGestureRecognizer(gesture)
        
        label.userInteractionEnabled = true
        
    }
    
    func wasDragged(gesture: UIPanGestureRecognizer) {
        
        let translation = gesture.translationInView(self.view)
        let label = gesture.view!
        
        label.center = CGPoint(x: self.view.bounds.width / 2 + translation.x, y: self.view.bounds.height / 2 + translation.y)
        //print(label.center.x, label.center.y)
        
        
        
        
        let xFromCenter = label.center.x - self.view.bounds.width / 2
        
        let scale = min(50 / abs(xFromCenter), 1)
        
        
        var rotation = CGAffineTransformMakeRotation(xFromCenter / 200)
        
        var stretch = CGAffineTransformScale(rotation, scale, scale)
        
        label.transform = stretch
        
        
        if gesture.state == UIGestureRecognizerState.Changed{
            
//            let centerStr: String = String(format: "%.2f-%.2f", label.center.x, label.center.y)
//            //print(centerStr)
//            NSNotificationCenter.defaultCenter().postNotificationName("CoordinateChanged", object: centerStr)
            
            let newTranslation: String = String(format: "%.2f,%.2f", translation.x, translation.y)
            print(newTranslation)
            NSNotificationCenter.defaultCenter().postNotificationName("CoordinateChanged", object: newTranslation)
            
            
        } else if gesture.state == UIGestureRecognizerState.Ended {
            
            rotation = CGAffineTransformMakeRotation(0)
            
            stretch = CGAffineTransformScale(rotation, 1, 1)
            
            label.transform = stretch
            
            label.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
            
            //let centerStr: String = String(format: "%.2f-%.2f", label.center.x, label.center.y)
           //= print("back to origin")
            let newTranslationOrigin : String = String(format: "%.2f,%.2f", 0, 0)
            NSNotificationCenter.defaultCenter().postNotificationName("CoordinateChanged", object: newTranslationOrigin)
            //NSNotificationCenter.defaultCenter().postNotificationName("CoordinateChangedToOrigin", object: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
