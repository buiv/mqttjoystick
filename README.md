# MQTT Joy Stick #

Version: 0.1.0

* A small application to demonstrate iOS working with other IoT devices using MQTT protocol 
* Use MQTTKit: https://github.com/jmesnil/MQTTKit (mosquito) 
* Allow Joy Stick Simulation using MQTT publisher/subscriber model
* Will work with real Joy Stick (Raspberry Pi)

![Screen Shot 2015-12-01 at 6.49.51 AM.png](https://bitbucket.org/repo/AdoELG/images/258558745-Screen%20Shot%202015-12-01%20at%206.49.51%20AM.png)

![Screen Shot 2015-12-01 at 6.50.30 AM.png](https://bitbucket.org/repo/AdoELG/images/3161774399-Screen%20Shot%202015-12-01%20at%206.50.30%20AM.png)

## Usage ##

For this version, use 2 or more iOS device (even simulator). One acts as publisher, drag the text (text will rotate and stretch), the same action will be display on subscribers' screen.

Author: Viet Bui